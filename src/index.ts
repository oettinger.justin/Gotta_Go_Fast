const gbas: Array<GBA> = [];
type Bit = 0 | 1;


class GBA {
    displayElement: Element | null = null;
    memory = new Uint8Array(1024 * 64); //check the actual memory size of the gba, fix here
    cpu = {
        registers: new Uint32Array(37),
        isThumbMode: false,
    };

    static ArmInstruction = class {
        bits: Array<Bit> = [];

        constructor(byte1: number, byte2: number, byte3: number, byte4: number) {
            this.appendByte(byte1);
            this.appendByte(byte2);
            this.appendByte(byte3);
            this.appendByte(byte4);
        }

        /**
         * Appends LSB first, MSB last to this.bits
         *
         * @param {number} byte is the byte to append
         */
        appendByte(byte: number) {
            for (let i = 0; i < 8; i++) {
                this.bits.push(<Bit>((byte >> i) & 1));
            }
        }

        execute(gba: GBA) {
            gba.cpu; //do something here
        }
    }

    static ThumbInstruction = class {
        bits: number = 0;

        constructor(byte1: number, byte2: number) {
            this.bits = (byte1 << 16) | byte2;
        }

        private execute(gba: GBA) {
            const top6bits = this.bits >> 10;
            switch (top6bits) {
                case 0b000000:
                case 0b000001:
                case 0b000010:
                case 0b000011:
                    return this.moveShifted(gba); //format 1
                case 0b000100:
                case 0b000101:
                case 0b000110:
                case 0b000111:
                    return this.addSubtract(gba); //format 2
                case 0b001000:
                case 0b001001:
                    return this.immedMove(gba); //format 3a
                case 0b001010:
                case 0b001011:
                    return this.immedCmp(gba); //format 3b
                case 0b001100:
                case 0b001101:
                    return this.immedAdd(gba); //format 3c
                case 0b001110:
                case 0b001111:
                    return this.immedSub(gba); //format 3d
                case 0b010000:
                    return this.aluops(gba); //format 4
                case 0b010001:
                    return this.hiregOps(gba); //format 5
                case 0b010010:
                case 0b010011:
                    return this.pcLoad(gba); //format 6
                case 0b010100:
                case 0b010101:
                case 0b010110:
                case 0b010111:
                    return (this.bits & 0b0000001000000000) === 0
                        ? this.lsRegOffset(gba) //format 7
                        : this.lsSignex(gba); //format 8
                case 0b011000:
                case 0b011001:
                case 0b011010:
                case 0b011011:
                case 0b011100:
                case 0b011101:
                case 0b011110:
                case 0b011111:
                    return this.lsImmOffset(gba); //format 9
                case 0b100000:
                case 0b100001:
                case 0b100010:
                case 0b100011:
                    return this.lsHalfword(gba); //format 10
                case 0b100100:
                case 0b100101:
                case 0b100110:
                case 0b100111:
                    return this.lsSPrel(gba); //format 11
                case 0b101000:
                case 0b101001:
                case 0b101010:
                case 0b101011:
                    return this.loadAddress(gba); //format 12
                case 0b101100:
                case 0b101101:
                case 0b101110:
                case 0b101111:
                    return (this.bits & 0b0000111100000000) === 0
                        ? this.addOffset(gba) //format 13
                        : this.pushPop(gba); //format 14
                case 0b110000:
                case 0b110001:
                case 0b110010:
                case 0b110011:
                    return this.multiLS(gba); //format 15
                case 0b110100:
                case 0b110101:
                case 0b110110:
                case 0b110111:
                    return (this.bits & 0b0000111100000000) !== 0
                        ? this.interrupt(gba) //format 16?
                        : this.condBranch(gba); //format 17?
                case 0b111000:
                case 0b111001:
                case 0b111010:
                case 0b111011:
                    return this.uncondBranch(gba); //format 19?
                case 0b111100:
                case 0b111101:
                case 0b111110:
                case 0b111111:
                    return this.longBranch(gba); //format 18?
            }
        }
        
        private moveShifted(gba: GBA) {
            const regFrom = this.bits[12] * 1 + this.bits[11] * 2 + this.bits[10] * 4;
            const regTo = this.bits[15] * 1 + this.bits[14] * 2 + this.bits[13] * 4;
            const offset = this.bits[9] * 1 + this.bits[8] * 2 + this.bits[7] * 4 + this.bits[6] * 8 + this.bits[5] * 16;
            const opcode = this.bits[4] * 1 + this.bits[3] * 2;
            let data = gba.cpu.registers[regFrom];
            if(opcode === 2){
                data = data >> offset;
            }
            else if(opcode === 1){
                data = data >>> offset;
            }
            else if(opcode === 0){
                data = data << offset;
            }
            gba.cpu.registers[regTo] = data;
        }

        private addSubtract(gba : GBA) {
            const regFrom = this.bits[12] * 1 + this.bits[11] * 2 + this.bits[10] * 4;
            const regTo = this.bits[15] * 1 + this.bits[14] * 2 + this.bits[13] * 4;
            const offset = this.bits[9] * 1 + this.bits[8] * 2 + this.bits[7] * 4;
            const opcode = this.bits[6];
            const immcode = this.bits[5];
            let data = gba.cpu.registers[regFrom];
            if(opcode === 0) {
                if (immcode === 0) {
                    data = data + gba.cpu.registers[offset];
                }
                if (immcode === 1) {
                    data = data + offset;
                }
            }
            if(opcode === 1) {
                if(immcode === 0) {
                    data = data - gba.cpu.registers[offset];
                }
                if(immcode === 1) {
                    data = data - offset;
                }
            }
            gba.cpu.registers[regTo] = data;
        }

        private immedMove(gba : GBA) {
            const regTo = this.bits[7] * 1 + this.bits[6] * 2 + this.bits[5] * 4;
            const offset = this.bits[15] * 1 + this.bits[14] * 2 + this.bits[13] * 4 + this.bits[12] * 8
                + this.bits[11] * 16 + this.bits[10] * 32 + this.bits[9] * 64 + this.bits[8] * 128;
            gba.cpu.registers[regTo] = offset;
        }

        private immedCmp (gba : GBA) {
            const regTo = this.bits[7] * 1 + this.bits[6] * 2 + this.bits[5] * 4;
            const offset = this.bits[15] * 1 + this.bits[14] * 2 + this.bits[13] * 4 + this.bits[12] * 8
                + this.bits[11] * 16 + this.bits[10] * 32 + this.bits[9] * 64 + this.bits[8] * 128;
            gba.cpu.registers[16] = gba.cpu.registers[regTo] - offset; //FINISH : check cmp reg
        }

        private immedAdd (gba : GBA) {
            const regTo = this.bits[7] * 1 + this.bits[6] * 2 + this.bits[5] * 4;
            const offset = this.bits[15] * 1 + this.bits[14] * 2 + this.bits[13] * 4 + this.bits[12] * 8
                + this.bits[11] * 16 + this.bits[10] * 32 + this.bits[9] * 64 + this.bits[8] * 128;
            gba.cpu.registers[regTo] = gba.cpu.registers[regTo] + offset;
        }

        private immedSub (gba : GBA) {
            const regTo = this.bits[7] * 1 + this.bits[6] * 2 + this.bits[5] * 4;
            const offset = this.bits[15] * 1 + this.bits[14] * 2 + this.bits[13] * 4 + this.bits[12] * 8
                + this.bits[11] * 16 + this.bits[10] * 32 + this.bits[9] * 64 + this.bits[8] * 128;
            gba.cpu.registers[regTo] = gba.cpu.registers[regTo] - offset;
        }

        private aluops(gba : GBA) {
            const regFrom = this.bits[12] * 1 + this.bits[11] * 2 + this.bits[10] * 4;
            const regTo = this.bits[15] * 1 + this.bits[14] * 2 + this.bits[13] * 4;
            const opcode = this.bits[9] * 1 + this.bits[8] * 2 + this.bits[7] * 4 + this.bits[6] * 8;
            switch(opcode) {
                case 0b0000:
                    gba.cpu.registers[regTo] = gba.cpu.registers[regTo] & gba.cpu.registers[regFrom];
                case 0b0001:
                    gba.cpu.registers[regTo] = gba.cpu.registers[regTo] ^ gba.cpu.registers[regFrom];
                case 0b0010:
                    gba.cpu.registers[regTo] = gba.cpu.registers[regTo] << gba.cpu.registers[regFrom];
                case 0b0011:
                case 0b0100:
                case 0b0101:
                case 0b0110:
                case 0b0111:
                case 0b1000:
                case 0b1001:
                case 0b1010:
                case 0b1011:
                case 0b1100:
                case 0b1101:
                case 0b1110:
                case 0b1111:
                //FINISH ALU OPS
            }
        }

        private hiregOps(gba : GBA) {
            const opcode = this.bits[7] * 1 + this.bits[6] * 2;
            const hiFrom = this.bits[9];
            const hiTo = this.bits[8];
            const regFrom = (this.bits[12] * 1 + this.bits[11] * 2 + this.bits[10] * 4) + (8 * hiFrom);
            const regTo = (this.bits[15] * 1 + this.bits[14] * 2 + this.bits[13] * 4) + (8 * hiTo);
            //FINISH : Check that hiFrom and hiTo are the correct bits
            if (opcode === 0) {
                gba.cpu.registers[regTo] = gba.cpu.registers[regTo] + gba.cpu.registers[regFrom];
            }
            if (opcode === 1) {
                gba.cpu.registers[16] = gba.cpu.registers[regTo] - gba.cpu.registers[regFrom];
                //FINISH : check cmp reg
            }
            if (opcode === 2) {
                gba.cpu.registers[regTo] = gba.cpu.registers[regFrom];
            }
            if (opcode === 3) {
                gba.cpu.registers[15] = gba.cpu.registers[regFrom];
            }
        }
        
        private pcLoad(gba : GBA) {
            const regTo = this.bits[7] * 1 + this.bits[6] * 2 + this.bits[5] * 4;
            const offset = this.bits[15] * 0 + this.bits[14] * 0 + this.bits[13] * 4 + this.bits[12] * 8
                + this.bits[11] * 16 + this.bits[10] * 32 + this.bits[9] * 64 + this.bits[8] * 128;
            const counter = gba.cpu.registers[15] //set PC bit 1 to 0
            //FINISH : check in documentation, says to set bits 1:0 to 0, figure out how to force PC[1] to 0
            gba.cpu.registers[regTo] = counter + offset;
        }

        private lsRegOffset(gba : GBA) {
            const regFrom = this.bits[12] * 1 + this.bits[11] * 2 + this.bits[10] * 4;
            const regTo = this.bits[15] * 1 + this.bits[14] * 2 + this.bits[13] * 4;
            const regOffset = this.bits[9] * 1 + this.bits[8] * 2 + this.bits[7] * 4;
            const byteWord = this.bits[5];
            const loadStore = this.bits[4];
            //FINISH : how to do byte vs word?
        }
    }

    //constructor
    constructor(el?: Element) {
        this.reset();
        if (el) {
            this.displayElement = el;
            this.setupDOM();
        }
    }

    //Functions
    private loadROM() {
        const bootloader = new Uint8Array(1024);//get rom from somewhere
        //create ROM const based on size
        //put ROM into memory some way or another
    }

    public cycle() {
        const pc = this.cpu.registers[32]; //or whatever PC actually is
        const instruction = this.cpu.isThumbMode
            ? new GBA.ThumbInstruction(this.memory[pc], this.memory[pc + 1])
            : new GBA.ArmInstruction(this.memory[pc], this.memory[pc + 1], this.memory[pc + 2], this.memory[pc + 3]);
        instruction.execute(this);
    }

    public run() {

    }

    public pause() {

    }

    public reset() {
        for (let reg of this.cpu.registers) {
            reg = 0;
        }
        this.cpu.isThumbMode = false;
    }

    setupDOM() {
        if (!this.displayElement) { return; }
        this.displayElement.innerHTML += `
        <div class="gbastate">
            <table class="registers">
                <thead>
                    <tr>
                        <td>Register</td>
                        <td>Value</td>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
        `;
    }

    stateIntervalCancelCode = 0;
    public startPrintingState() {
        if (this.stateIntervalCancelCode > 0) { return; }
        this.stateIntervalCancelCode = window.setInterval(() => {
            if (!this.displayElement) { return this.stopPrintingState(); }
            let table = '<tr>';
            for (const i in this.cpu.registers) {
                table += `<tr><td>${i}</td><td>${this.cpu.registers[i]}</td></tr>`
            }
            const registerTable = this.displayElement.querySelector('.gbastate .registers tbody');
            if (registerTable) { registerTable.innerHTML = table; }

        }, 500); //every half-second
    }

    public stopPrintingState() {
        window.clearInterval(this.stateIntervalCancelCode);
    }
}

//actually intialize each gba
(function () {
    const docReady = new Promise(resolve => {
        if (document.readyState !== "loading") {
            return resolve();
        } else {
            document.addEventListener("DOMContentLoaded", () => resolve());
        }
    });

    docReady.then(() => {
        const gbaEls = document.querySelectorAll('.gameboy');
        gbaEls.forEach(el => {
            gbas.push(new GBA(el));
        });
    });
})();